import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class PostService {

  private readonly API: string = `https://la-lopezerie-e3e1e.firebaseio.com/`

  public postsSubject: Subject<any>  = new Subject<any>()

  private posts: any[] = [];

  constructor(private http: HttpClient) { }

  emitPostSubject() {
    this.postsSubject.next(this.posts);
  }

  findById(id: string) {
    return this.posts.find((post) => post.id === id);
  }

  // récupère tous les posts

  getPosts() {
    this.http.get<any[]>(`${this.API}/posts.json`)
      .subscribe(
        data => {
          const posts = [];
          for (const [id, post] of Object.entries(data)) {
            post.id = id;
            post.date = Date.parse(post.date);
            posts.push(post);
          }
          this.posts = posts;
          this.emitPostSubject();
        },
        error => { console.error(error); },
        () => { console.log('GET posts complete'); }
      );
  }

  // récupère une annonce en particulier

  getPost(id: string) {
    this.http.get<any>(`${this.API}/posts/${id}.json`)
      .subscribe(
        post => {
          post.id = id
          post.date = Date.parse(post.date)
          this.insertOrUpdate(post)
          this.emitPostSubject();
        },
        error => { console.error('PostService.getPosts(): ', error); },
        () => { console.info('PostService.getPosts(): complete'); }
      );
  }

  // ajouter un post

  public addPost(post) {
    return this.http.post<{name}>(`${this.API}/posts.json`, post).subscribe(
      data => {
        post.id = data.name
        this.posts.push(post)
        this.emitPostSubject();
      },
      error => console.error('PostService.addPost(): ', error),
      () => console.info('PostService.addPost(): complete')
    );
  }

  // insert ou met à jour une annonce
  private insertOrUpdate(newPost) {
    const index = this.posts.findIndex(post => post.id === newPost.id); // cherche l'index de l'annonce dans le tableau posts
    if (index > -1) {
      this.posts[index] = newPost; // remplacement de l'annonce existante
    } else {
      this.posts.push(newPost); // ajout d'une nouvelle annonce
    }
  }

  like(i) {
    this.posts[i].loveIts++;
  }

  dislike(i) {
    this.posts[i].loveIts--;
  }

  delete(id) {
    const index = this.posts.map(function(x) { return x.id; }).indexOf(id);
    this.posts.splice(index, 1);
  }

}

import {Component, Input, OnInit} from '@angular/core';
import { PostService } from '../services/post.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-list-view',
  templateUrl: './post-list-view.component.html',
  styleUrls: ['./post-list-view.component.scss']
})
export class PostListViewComponent implements OnInit {

  @Input() posts: any;
  @Input() title: string;
  @Input() content: string;
  @Input() created_ad: Date;
  @Input() loveIts: number;
  @Input() index: number;
  @Input() id: number;

  constructor(private postService: PostService, private router: Router) { }

  ngOnInit() {
    console.log(this.postService)
    this.postService.postsSubject.subscribe(
      (ads) => this.posts = ads
    )
    this.postService.getPosts();
  }



}

import {Component, Input, OnInit} from '@angular/core';
import { PostService } from '../services/post.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit {

  private id: string
  private post: {}

  constructor(private postService: PostService, private route: ActivatedRoute) { }

    ngOnInit() {
      this.id = String(this.route.snapshot.params['id'])
      this.postService.postsSubject.subscribe(
        (posts) => {
          this.post = this.postService.findById(this.id);
        }
      )
      this.postService.getPost(this.id);
    }

}
